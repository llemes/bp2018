INSERT INTO italy_wine_reviews(points, price, province, region_1, region_2, taster_name, taster_twitter_handle, title, variety, winery)
VALUES(87, NULL, 'Sicily \& Sardinia', 'Etna', NULL, 'Kerin O�Keefe', '@kerinokeefe', 'Nicosia 2013 Vulka Bianco  (Etna)', 'White Blend', 'Nicosia');

INSERT INTO italy_wine_reviews(points, price, province, region_1, region_2, taster_name, taster_twitter_handle, title, variety, winery)
VALUES(87, 16, 'Sicily \& Sardinia', 'Vittoria', NULL, 'Kerin O�Keefe', '@kerinokeefe', 'Terre di Giurfo 2013 Belsito Frappato (Vittoria)', 'Frappato', 'Terre di Giurfo');

INSERT INTO italy_wine_reviews(points, price, province, region_1, region_2, taster_name, taster_twitter_handle, title, variety, winery)
VALUES(87, NULL, 'Sicily \& Sardinia', 'Etna', NULL, 'Kerin O�Keefe', '@kerinokeefe', 'Masseria Setteporte 2012 Rosso  (Etna)', 'Nerello Mascalese', 'Masseria Setteporte');

INSERT INTO italy_wine_reviews(points, price, province, region_1, region_2, taster_name, taster_twitter_handle, title, variety, winery)
VALUES(87, 19, 'Sicily \& Sardinia', 'Sicilia', NULL, 'Kerin O�Keefe', '@kerinokeefe', 'Baglio di Pianetto 2007 Ficiligno White (Sicilia)', 'White Blend', 'Baglio di Pianetto');

INSERT INTO italy_wine_reviews(points, price, province, region_1, region_2, taster_name, taster_twitter_handle, title, variety, winery)
VALUES(87, 35, 'Sicily \& Sardinia', 'Sicilia', NULL, 'Kerin O�Keefe', '@kerinokeefe', 'Canicatti 2009 Aynat Nero dAvola (Sicilia)', 'Nero dAvola', 'Canicatti');

INSERT INTO italy_wine_reviews(points, price, province, region_1, region_2, taster_name, taster_twitter_handle, title, variety, winery)
VALUES(87, 13, 'Sicily \& Sardinia', 'Terre Siciliane', NULL, 'Kerin O�Keefe', '@kerinokeefe', 'Stemmari 2013 Dalila White (Terre Siciliane)', 'White Blend', 'Stemmari');

INSERT INTO italy_wine_reviews(points, price, province, region_1, region_2, taster_name, taster_twitter_handle, title, variety, winery)
VALUES(87, 10, 'Sicily \& Sardinia', 'Terre Siciliane', NULL, 'Kerin O�Keefe', '@kerinokeefe', 'Stemmari 2013 Nero dAvola (Terre Siciliane)', 'Nero dAvola', 'Stemmari');

INSERT INTO italy_wine_reviews(points, price, province, region_1, region_2, taster_name, taster_twitter_handle, title, variety, winery)
VALUES(87, 17, 'Sicily \& Sardinia', 'Cerasuolo di Vittoria', NULL, 'Kerin O�Keefe', '@kerinokeefe', 'Terre di Giurfo 2011 Mascaria Barricato  (Cerasuolo di Vittoria)', 'Red Blend', 'Terre di Giurfo');

INSERT INTO italy_wine_reviews(points, price, province, region_1, region_2, taster_name, taster_twitter_handle, title, variety, winery)
VALUES(86, NULL, 'Sicily \& Sardinia', 'Sicilia', NULL, NULL, NULL, 'Duca di Salaparuta 2010 Calanica Nero dAvola-Merlot Red (Sicilia)', 'Red Blend', 'Duca di Salaparuta');

INSERT INTO italy_wine_reviews(points, price, province, region_1, region_2, taster_name, taster_twitter_handle, title, variety, winery)
VALUES(86, NULL, 'Sicily \& Sardinia', 'Sicilia', NULL, NULL, NULL, 'Duca di Salaparuta 2011 Calanica Grillo-Viognier White (Sicilia)', 'White Blend', 'Duca di Salaparuta');

SELECT * FROM italy_wine_reviews;