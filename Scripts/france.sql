INSERT INTO france_wine_reviews(points, price, province, region_1, region_2, taster_name, taster_twitter_handle, title, variety, winery, description_id)
VALUES(87, 24, 'Alsace', 'Alsace', NULL, 'Roger Voss', '@vossroger', 'Trimbach 2012 Gewurztraminer (Alsace)', 'Gewurztraminer', 'Trimbach', '5bdf54bbe7179a1b53029c38');

INSERT INTO france_wine_reviews(points, price, province, region_1, region_2, taster_name, taster_twitter_handle, title, variety, winery, description_id)
VALUES(87, 27, 'Alsace', 'Alsace', NULL, 'Roger Voss', '@vossroger', 'Jean-Baptiste Adam 2012 Les Natures Pinot Gris (Alsace)', 'Pinot Gris', 'Jean-Baptiste Adam', '5bdf5541e7179a1b53029c4c');

INSERT INTO france_wine_reviews(points, price, province, region_1, region_2, taster_name, taster_twitter_handle, title, variety, winery, description_id)
VALUES(87, 30, 'Alsace', 'Alsace', NULL, 'Roger Voss', '@vossroger', 'Leon Beyer 2012 Gewurztraminer (Alsace)', 'Gewurztraminer', 'Leon Beyer', '5bdf5562e7179a1b53029c50');

INSERT INTO france_wine_reviews(points, price, province, region_1, region_2, taster_name, taster_twitter_handle, title, variety, winery, description_id)
VALUES(86, NULL, 'Beaujolais', 'Beaujolais-Villages', NULL, 'Roger Voss', '@vossroger', 'Domaine de la Madone 2012 Nouveau  (Beaujolais-Villages)', 'Gamay', 'Domaine de la Madone', '5bdf558de7179a1b53029c5a');

INSERT INTO france_wine_reviews(points, price, province, region_1, region_2, taster_name, taster_twitter_handle, title, variety, winery, description_id)
VALUES(86, 9, 'Beaujolais', 'Beaujolais', NULL, 'Roger Voss', '@vossroger', 'Henry Fessy 2012 Nouveau  (Beaujolais)', 'Gamay', 'Henry Fessy', '5bdf55a9e7179a1b53029c5f');

INSERT INTO france_wine_reviews(points, price, province, region_1, region_2, taster_name, taster_twitter_handle, title, variety, winery, description_id)
VALUES(86, 14, 'Beaujolais', 'Brouilly', NULL, 'Roger Voss', '@vossroger', 'Vignerons de Bel Air 2011 Et� Indien  (Brouilly)', 'Gamay', 'Vignerons de Bel Air', '5bdf55c1e7179a1b53029c62');

INSERT INTO france_wine_reviews(points, price, province, region_1, region_2, taster_name, taster_twitter_handle, title, variety, winery, description_id)
VALUES(85, 15, 'Bordeaux', 'Bordeaux Blanc', NULL, 'Roger Voss', '@vossroger', 'Ch�teau de Sours 2011 La Fleur dAm�lie  (Bordeaux Blanc)', 'Bordeaux-style White Blend', 'Chateau de Sours', '5bdf55d5e7179a1b53029c63');

INSERT INTO france_wine_reviews(points, price, province, region_1, region_2, taster_name, taster_twitter_handle, title, variety, winery, description_id)
VALUES(86, 58, 'Champagne', 'Champagne', NULL, 'Roger Voss', '@vossroger', 'Roland Champion NV Brut Ros�  (Champagne)', 'Champagne Blend', 'Roland Champion', '5bdf55e8e7179a1b53029c65');

INSERT INTO france_wine_reviews(points, price, province, region_1, region_2, taster_name, taster_twitter_handle, title, variety, winery, description_id)
VALUES(86, 24, 'Burgundy', 'Chablis', NULL, 'Roger Voss', '@vossroger', 'Simonnet-Febvre 2015  Chablis', 'Chardonnay', 'Simonnet-Febvre', '5bdf55fce7179a1b53029c67');

INSERT INTO france_wine_reviews(points, price, province, region_1, region_2, taster_name, taster_twitter_handle, title, variety, winery, description_id)
VALUES(86, 15, 'Burgundy', 'Macon-Milly Lamartine', NULL, 'Roger Voss', '@vossroger', 'Vignerons des Terres Secretes 2015  M�con-Milly Lamartine', 'Chardonnay', 'Vignerons des Terres Secretes', '5bdf5612e7179a1b53029c6c');

SELECT * FROM france_wine_reviews;