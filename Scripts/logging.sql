SELECT * FROM spain_wine_reviews_log_entries

CREATE TABLE spain_wine_reviews_log_entries AS SELECT * FROM spain_wine_reviews

CREATE TABLE spain_wine_reviews_logs(
  log_id INTEGER NOT NULL,
  insert_timestamp TIMESTAMP NOT NULL,
  log_entry_id INTEGER NOT NULL,
  database_user VARCHAR(255) NOT NULL,
  CONSTRAINT spain_logs_pk PRIMARY KEY (log_id));

CREATE SEQUENCE s_log_id
NOCACHE
NOCYCLE;

CREATE OR REPLACE TRIGGER s_log_id_pk_autoincrement
BEFORE INSERT
ON spain_wine_reviews_logs
FOR EACH ROW
BEGIN

SELECT s_log_id.NEXTVAL INTO :new.log_id FROM dual;

END;
/

CREATE OR REPLACE TRIGGER spain_wine_reviews_log_entry
BEFORE UPDATE
ON spain_wine_reviews
FOR EACH ROW
DECLARE
current_time TIMESTAMP;
current_user VARCHAR(255);
BEGIN

SELECT current_timestamp INTO current_time FROM dual;
SELECT USER INTO current_user FROM dual ;

-- only log updates
IF :old.review_id IS NOT NULL
THEN

  INSERT INTO spain_wine_reviews_log_entries(
    review_id,
    winery,
    variety,
    title,
    taster_twitter_handle,
    taster_name,
    region_1,
    region_2,
    province,
    price,
    points,
    designation,
    description_id
  )
  VALUES (
    :old.review_id,
    :old.winery,
    :old.variety,
    :old.title,
    :old.taster_twitter_handle,
    :old.taster_name,
    :old.region_1,
    :old.region_2,
    :old.province,
    :old.price,
    :old.points,
    :old.designation,
    :old.description_id
  );

  INSERT INTO spain_wine_reviews_logs(insert_timestamp, log_entry_id, database_user)
  VALUES(current_time, :old.review_id, current_user);

END IF;
END;
/


UPDATE spain_wine_reviews
SET points = 666;

SELECT * FROM spain_wine_reviews_log_entries
