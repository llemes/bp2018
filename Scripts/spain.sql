SELECT * FROM spain_wine_reviews;

INSERT INTO spain_wine_reviews(points, price, province, region_1, region_2, taster_name, taster_twitter_handle, title, variety, winery)
VALUES(87, 15, 'Northern Spain', 'Navarra', NULL, 'Michael Schachner', '@wineschach', 'Tandem 2011 Ars In Vitro Tempranillo-Merlot (Navarra)', 'Tempranillo-Merlot', 'Tandem');

INSERT INTO spain_wine_reviews(points, price, province, region_1, region_2, taster_name, taster_twitter_handle, title, variety, winery)
VALUES(87, 28, 'Northern Spain', 'Ribera del Duero', NULL, 'Michael Schachner', '@wineschach', 'Pradorey 2010 Vendimia Seleccionada Finca Valdelayegua Single Vineyard Crianza  (Ribera del Duero)', 'Tempranillo Blend', 'Pradorey');

INSERT INTO spain_wine_reviews(points, price, province, region_1, region_2, taster_name, taster_twitter_handle, title, variety, winery)
VALUES(86, 16, 'Galicia', 'R�as Baixas', NULL, 'Michael Schachner', '@wineschach', 'Spyro 2014 Albarino (R�as Baixas)', 'Albarino', 'Spyro');

INSERT INTO spain_wine_reviews(points, price, province, region_1, region_2, taster_name, taster_twitter_handle, title, variety, winery)
VALUES(91, 50, 'Central Spain', 'Dominio de Valdepusa', NULL, 'Michael Schachner', '@wineschach', 'Marques de Grinon 2010 Single Vineyard Estate Bottled Graciano (Dominio de Valdepusa)', 'Graciano', 'Marques de Grinon');

INSERT INTO spain_wine_reviews(points, price, province, region_1, region_2, taster_name, taster_twitter_handle, title, variety, winery)
VALUES(90, 40, 'Northern Spain', 'Rueda', NULL, 'Michael Schachner', '@wineschach', 'Matarromera 2015 Fermentado en Barrica Verdejo (Rueda)', 'Verdejo', 'Matarromera');

INSERT INTO spain_wine_reviews(points, price, province, region_1, region_2, taster_name, taster_twitter_handle, title, variety, winery)
VALUES(85, 13, 'Northern Spain', 'Vino de la Tierra de Castilla y Le�n', NULL, 'Michael Schachner', '@wineschach', 'J. \& F. Lurton 2007 Herederos de Fran�ois Lurton Villafrance de Duero Red (Vino de la Tierra de Castilla y Le�n)', 'Red Blend', 'J. \& F. Lurton');

INSERT INTO spain_wine_reviews(points, price, province, region_1, region_2, taster_name, taster_twitter_handle, title, variety, winery)
VALUES(92, 40, 'Catalonia', 'Montsant', NULL, 'Michael Schachner', '@wineschach', 'Ac�stic 2010 Bra� Vinyes Velles Carignan-Grenache (Montsant)', 'Carignan-Grenache', 'Ac�stic');

INSERT INTO spain_wine_reviews(points, price, province, region_1, region_2, taster_name, taster_twitter_handle, title, variety, winery)
VALUES(82, 13, 'Catalonia', 'Cava', NULL, 'Michael Schachner', '@wineschach', 'Cavas Hill NV 1887 Rosado Sparkling (Cava)', 'Sparkling Blend', 'Cavas Hill');

INSERT INTO spain_wine_reviews(points, price, province, region_1, region_2, taster_name, taster_twitter_handle, title, variety, winery)
VALUES(82, 11, 'Catalonia', 'Cava', NULL, 'Michael Schachner', '@wineschach', 'Bellisco NV Sparkling (Cava)', 'Sparkling Blend', 'Bellisco');

INSERT INTO spain_wine_reviews(points, price, province, region_1, region_2, taster_name, taster_twitter_handle, title, variety, winery)
VALUES(89, 18, 'Northern Spain', 'Rioja', NULL, 'Michael Schachner', '@wineschach', 'Luberri 2011 Seis  (Rioja)', 'Tempranillo', 'Luberri');