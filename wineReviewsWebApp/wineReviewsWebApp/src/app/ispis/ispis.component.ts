import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Wine } from '../models/wine.model';

@Component({
  selector: 'app-ispis',
  templateUrl: './ispis.component.html',
  styleUrls: ['./ispis.component.scss']
})
export class IspisComponent implements OnInit {
  id;
  model: Wine = new Wine();
  
  constructor(private http: HttpClient) { }

  ngOnInit() {
    console.log(this.model);
  }

  getData(){
    var header = new HttpHeaders();
    header.append('Content-Type', 'application/json');
    let body = new HttpParams();
    console.log(typeof(this.id));
    body=body.set('id', this.id );
    this.http.post('http://localhost:8080/getData', body, {headers:header}).subscribe((data:any) =>{
      console.log(data);
      // var jsonData = data.json()
      // console.log(jsonData);
      this.model = new Wine(data);

    } , error => console.log(error));

  }

}
