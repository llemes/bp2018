import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
// import { UnosComponent } from './unos/unos.component';
// import { FormsModule, FormGroup, ReactiveFormsModule }   from '@angular/forms';
import {FormsModule}  from '@angular/forms';
import { IspisComponent } from './ispis/ispis.component';

@NgModule({
  declarations: [
    AppComponent,
    // UnosComponent
    routingComponents,
    IspisComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    // FormGroup,
    // ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
