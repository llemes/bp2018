import { Component, OnInit } from '@angular/core';
import {Wine} from '../models/wine.model';
// import { FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-unos',
  templateUrl: './unos.component.html',
  styleUrls: ['./unos.component.scss']
})
export class UnosComponent implements OnInit {
  // form: FormGroup;
  model: Wine = new Wine()
  constructor(private http: HttpClient) {
    // this.createForm();
   }

  //  createForm() {
  //   this.form = this.fb.group({
  //     name: ['', Validators.required],
  //     avatar: null
  //   });
  // }

  ngOnInit() {
  }

  ispisi(){
    console.log(this.model);
    var header = new HttpHeaders();
      header.append('Content-Type', 'application/json');
      let body = new HttpParams();
      body=body.set('winery', this.model.winery );
      body=body.set('variety', this.model.variety);
      body=body.set('title', this.model.title);
      body=body.set('tasterTwitterHandle', this.model.tasterTwitterHandle);
      body=body.set('tasterName', this.model.tasterName);
      body=body.set('region1', this.model.region1);
      body=body.set('region2', this.model.region2);
      body=body.set('province', this.model.province);      
      body=body.set('price', this.model.price.toString());
      body=body.set('points', this.model.points.toString());
      body=body.set('designation', this.model.designation);
      
      
   this.http.post('http://localhost:8080/saveData', body,{headers: header}).subscribe(data => console.log(data), error => console.log(error));

  }
  pom;
  onFilesAdded(event){
    // let reader = new FileReader();
    // if(event.target.files && event.target.files.length > 0) {
    //   let file = event.target.files[0];
    //   reader.readAsDataURL(file);
    //   console.log(reader);
    //   reader.onload = () => {
    //     this.pom = (<string>reader.result).split(',')[1]
        
    //   };
    // }
  }

}
