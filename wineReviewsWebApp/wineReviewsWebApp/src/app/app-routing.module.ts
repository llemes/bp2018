import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UnosComponent } from './unos/unos.component';
import { IspisComponent } from './ispis/ispis.component';

const routes: Routes = [
  {path:'unos', component: UnosComponent},
  {path:'ispis', component:IspisComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [UnosComponent]