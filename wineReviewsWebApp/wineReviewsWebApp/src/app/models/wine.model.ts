export class Wine {
    winery: string;
    variety: string;
    title: string;
    tasterTwitterHandle: string;
    tasterName: string;
    region1: string;
    region2: string;
    province: string;
    price: number;
    points:number;
    designation:string;
    
    /**
     *
     */
    constructor(obj?:any) {
      if (obj!== undefined){
        this.winery = obj.winery;
        this.variety=obj.variety;
        this.title=obj.title;
        this.tasterTwitterHandle=obj.tasterTwitterHandle;
        this.tasterName=obj.tasterName;
        this.region1=obj.region1;
        this.region2=obj.region2;
        this.province=obj.province;
        this.price=obj.price;
        this.points=obj.points;
        this.designation=obj.designation;
      }

    }

  }