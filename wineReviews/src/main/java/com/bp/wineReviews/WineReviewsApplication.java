// package com.bp.wineReviews;

// import java.util.Iterator;
// import java.util.Properties;
// import java.util.List;

// import javax.persistence.GeneratedValue;
// import javax.persistence.Id;

// import com.bp.wineReviews.Models.SpainWineReviews;

// import org.hibernate.HibernateException;
// import org.hibernate.Session;
// import org.hibernate.SessionFactory;
// import org.hibernate.Transaction;
// import org.hibernate.annotations.Entity;
// import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
// import org.hibernate.cfg.Configuration;
// import org.springframework.boot.SpringApplication;
// import org.springframework.boot.autoconfigure.SpringBootApplication;
// import org.springframework.web.bind.annotation.RequestMapping;

// @SpringBootApplication
// public class WineReviewsApplication {
// 	private static SessionFactory factory; 
// 	public static void main(String[] args) {
	   
// 	   try {
// 		  factory = new Configuration().configure().buildSessionFactory();
// 	   } catch (Throwable ex) { 
// 		  System.err.println("Failed to create sessionFactory object." + ex);
// 		  throw new ExceptionInInitializerError(ex); 
// 	   }
	   
// 	   WineReviewsApplication ME = new WineReviewsApplication();
 
// 	   /* Add few employee records in database */
// 	   Integer empID1 = ME.addReview(1,"Nestooo", "Vino neko", "Super vino", "twitter", "Amila", "regija 1", "regija 2", "provincija", 1000, 7, "mmm", 18);
// 	   Integer empID2 = ME.addReview(2,"Nestooo1", "Vino neko1", "Super vino", "twitter", "Amila", "regija 1", "regija 2", "provincija", 1000, 7, "mmm", 18);
// 	   /* List down all the employees */
// 	   //ME.listEmployees();
 
// 	   /* Update employee's records */
// 	   //ME.updateEmployee(empID1, 5000);
 
// 	   /* Delete an employee from the database */
// 	   //ME.deleteEmployee(empID2);
 
// 	   /* List down new list of the employees */
// 	   ME.listEmployees();
// 	}
	
// 	/* Method to CREATE an employee in the database */
// 	public Integer addReview(Integer id,String winery, String variety, String title, String twh, String taster, String region1, String region2, String province, int price, int points, String designation, int description_id){
// 	   Session session = factory.openSession();
// 	   Transaction tx = null;
// 	   Integer employeeID = null;
	   
// 	   try {
// 		  tx = session.beginTransaction();
// 		  SpainWineReviews employee = new SpainWineReviews(id, winery, variety, title,twh, taster, region1,region2,province,price,points,designation,description_id);
// 		  employeeID = (Integer) session.save(employee); 
// 		  tx.commit();
// 	   } catch (HibernateException e) {
// 		  if (tx!=null) tx.rollback();
// 		  e.printStackTrace(); 
// 	   } finally {
// 		  session.close(); 
// 	   }
// 	   return employeeID;
// 	}
	
// 	/* Method to  READ all the employees */
// 	public void listEmployees( ){
// 	   Session session = factory.openSession();
// 	   Transaction tx = null;
	   
// 	   try {
// 		  tx = session.beginTransaction();
// 		  List employees = session.createQuery("FROM SpainWineReviews").list(); 
// 		  for (Iterator iterator = employees.iterator(); iterator.hasNext();){
// 			 SpainWineReviews employee = (SpainWineReviews) iterator.next(); 
// 			 System.out.print("Winery: " + employee.getWinery()); 
// 			 System.out.print("  Variety: " + employee.getVariety()); 
// 			 System.out.println("  Title: " + employee.getTitle()); 
// 		  }
// 		  tx.commit();
// 	   } catch (HibernateException e) {
// 		  if (tx!=null) tx.rollback();
// 		  e.printStackTrace(); 
// 	   } finally {
// 		  session.close(); 
// 	   }
// 	}
	
// 	/* Method to UPDATE salary for an employee */
// 	public void updateEmployee(Integer EmployeeID, int salary ){
// 	   Session session = factory.openSession();
// 	   Transaction tx = null;
	   
// 	   try {
// 		  tx = session.beginTransaction();
// 		  SpainWineReviews employee = (SpainWineReviews)session.get(SpainWineReviews.class, EmployeeID); 
// 		  employee.setPrice( salary );
// 		  session.update(employee); 
// 		  tx.commit();
// 	   } catch (HibernateException e) {
// 		  if (tx!=null) tx.rollback();
// 		  e.printStackTrace(); 
// 	   } finally {
// 		  session.close(); 
// 	   }
// 	}
	
// 	/* Method to DELETE an employee from the records */
// 	public void deleteEmployee(Integer EmployeeID){
// 	   Session session = factory.openSession();
// 	   Transaction tx = null;
	   
// 	   try {
// 		  tx = session.beginTransaction();
// 		  SpainWineReviews employee = (SpainWineReviews)session.get(SpainWineReviews.class, EmployeeID); 
// 		  session.delete(employee); 
// 		  tx.commit();
// 	   } catch (HibernateException e) {
// 		  if (tx!=null) tx.rollback();
// 		  e.printStackTrace(); 
// 	   } finally {
// 		  session.close(); 
// 	   }
// 	}


// }

package com.bp.wineReviews;

import java.util.Iterator;
import java.util.Properties;
import java.util.List;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.bp.wineReviews.Models.SpainWineReviews;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.annotations.Entity;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;


@SpringBootApplication
public class WineReviewsApplication {
	private static SessionFactory factory; 
    public static void main(String[] args) {
		SpringApplication.run(WineReviewsApplication.class, args);
	// 	try {
	// 	  factory = new Configuration().configure().buildSessionFactory();
	//    } catch (Throwable ex) { 
	// 	  System.err.println("Failed to create sessionFactory object." + ex);
	// 	  throw new ExceptionInInitializerError(ex); 
	//    }
    }
}
