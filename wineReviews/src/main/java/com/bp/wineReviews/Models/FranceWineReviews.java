package com.bp.wineReviews.Models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * SpainWineReviews
 */
@Entity
@Table(name = "France_Wine_Reviews")
public class FranceWineReviews {
    @Id
    @GeneratedValue
    Integer reviewId;
    String winery;
    String variety;
    String title;
    String tasterTwitterHandle;
    String tasterName;
    String region1;
    String region2;
    String province;
    Integer price;
    Integer points;
    String designation;
    Integer descriptionId;
    public FranceWineReviews() {}
    public FranceWineReviews(Integer reviewid,String winery,String variety, String title, String taster_twitter, String taster_name, String region1, String region2, String province, Integer price, Integer points, String designation, Integer description) {
        this.reviewId = reviewid;
        this.winery = winery;
        this.variety = variety;
        this.title = title;
        this.tasterTwitterHandle = taster_twitter;
        this.tasterName = taster_name;
        this.region1 = region1;
        this.region2 = region2;
        this.province = province;
        this.price  = price;
        this.points = points;
        this.designation = designation;
        this.descriptionId = description;
    }
    
    public Integer getReviewId() {
        return reviewId;
    }
    public void setReviewId(Integer review_id) {
        this.reviewId = review_id;
    }

    public String getWinery() {
        return winery;
    }
    public void setWinery(String winery) {
        this.winery = winery;
    }

    public String getVariety() {
        return variety;
    }
    public void setVariety(String variety) {
        this.variety = variety;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTasterTwitterHandle(String twh) {
        this.tasterTwitterHandle = twh;
    }

    public String getTasterTwitterHandle() {
        return this.tasterTwitterHandle;
    }

    public void setTasterName(String name) {
        this.tasterName = name;
    }

    public String getTasterName() {
        return this.tasterName;
    }

    public void setRegion1(String region1) {
        this.region1 = region1;
    }

    public String getRegion1() {
        return this.region1;
    }


    public void setRegion2(String region2) {
        this.region2 = region2;
    }

    public String getRegion2() {
        return this.region2;
    }


    public void setProvince(String province) {
        this.province = province;
    }

    public String getProvince() {
        return this.province;
    }


    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getPrice() {
        return this.price;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Integer getPoints() {
        return this.points;
    }

    public void setDescriptionId(Integer id) {
        this.descriptionId = id;
    }

    public Integer getDescriptionId() {
        return this.descriptionId;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getDesignation() {
        return this.designation;
    }
}