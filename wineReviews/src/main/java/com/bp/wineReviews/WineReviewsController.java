
package com.bp.wineReviews;

import java.util.Iterator;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicLong;
import java.util.List;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.bp.wineReviews.Models.SpainWineReviews;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.annotations.Entity;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class WineReviewsController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();


    // @RequestMapping("/greeting")
    // public Greeting greeting(@RequestParam(value="name", defaultValue="World") String name) {
    // 	String csvFile = "C:\\Users\\KORISNIK\\Desktop\\winemag-data-130k-v2.csv";
    //     BufferedReader br = null;
    //     String line = "";
    //     String cvsSplitBy = ",";
    	
    //     return new Greeting(5,
    //                         String.format(template, name));
    // }

    // @RequestMapping("/file")
    // public String str() {
    // 	String csvFile = "C:\\Users\\KORISNIK\\Desktop\\winemag-data-130k-v2.csv";
    //     BufferedReader br = null;
    //     String line = "";
    //     String cvsSplitBy = ",";
    // 	String finalVal ="[";
    //     try {

    //         br = new BufferedReader(new FileReader(csvFile));
    //         //while ((line = br.readLine()) != null) {
    //         int numOfRows = 10;
            
    //         for (int i=0;i<numOfRows ;i++) {
    //             // use comma as separator
    //         	line=br.readLine();
    //             String[] row = line.split(cvsSplitBy);
    //             if (i!=0) {
    //                 finalVal = finalVal + "{\"id\":" + row[0] + ",\"country\":\"" + row[1]+ "\",\"description\":\"";
    //                 String description = row[2].substring(1, row[2].length());
    //                 int brojac = 3;
    //                 if (!description.contains("\"")) {
    //                 	while (!row[brojac].contains("\"")) {
    //                     	description = description + row[brojac];
    //                     	System.out.println(row[brojac]);
    //                     	brojac++;
    //                     }
    //                 }
                    
    //                 description = description + row[brojac].substring(0, row[brojac].length()-2);
    //                 finalVal = finalVal + description + "\",\"designation\":\"" + row[brojac+1] + "\",\"points\":\"" + row[brojac+2] +"\",\"price\":\"" + row[brojac+3] + "\",\"province\":\"" + row[brojac+4] + "\",\"region_1\":\"" + row[brojac+5] + "\",\"region_2\":\"" + row[brojac+6] + "\",\"taster_name\":\"" + row[brojac+7] + "\",\"taster_twitter_handle\":\"" + row[brojac+8] + "\",\"tittle\":\"" + row[brojac+9]  + "\",\"variety\":\"" + row[brojac+10] + "\",\"winery\":\"" + row[brojac+11]+  "\"}";
    //                 if (i!=numOfRows-1) {
    //                 	finalVal = finalVal + ",";
    //                 }
    //             }
                

    //         }
    //         finalVal = finalVal + "]";
    //     } catch (FileNotFoundException e) {
    //         e.printStackTrace();
    //     } catch (IOException e) {
    //         e.printStackTrace();
    //     } finally {
    //         if (br != null) {
    //             try {
    //                 br.close();
    //             } catch (IOException e) {
    //                 e.printStackTrace();
    //             }
    //         }
    //     }
        
    //     return finalVal;
    // }
    private static SessionFactory factory; 
    
    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping("/poz")
    public String poz(){

        try {
            factory = new Configuration().configure().buildSessionFactory();
         } catch (Throwable ex) { 
            System.err.println("Failed to create sessionFactory object." + ex);
            throw new ExceptionInInitializerError(ex); 
         }

        Session session = factory.openSession();
	   Transaction tx = null;
	   
	   try {
		  tx = session.beginTransaction();
		  List employees = session.createQuery("FROM SpainWineReviews").list(); 
		  for (Iterator iterator = employees.iterator(); iterator.hasNext();){
			 SpainWineReviews employee = (SpainWineReviews) iterator.next(); 
			 System.out.print("Winery: " + employee.getWinery()); 
			 System.out.print("  Variety: " + employee.getVariety()); 
			 System.out.println("  Title: " + employee.getTitle()); 
		  }
          tx.commit();
          return "proslo";
	   } catch (HibernateException e) {
		  if (tx!=null) tx.rollback();
          e.printStackTrace(); 
          return "palo";     
	   } finally {
		  session.close(); 
       }
    //   return "{\"msg\":\"proslo\"}";
    }


	/* Method to CREATE an employee in the database */
	public Integer addReview(Integer id,String winery, String variety, String title, String twh, String taster, String region1, String region2, String province, int price, int points, String designation, int description_id){
	   Session session = factory.openSession();
	   Transaction tx = null;
	   Integer employeeID = null;
	   
	   try {
		  tx = session.beginTransaction();
		  SpainWineReviews employee = new SpainWineReviews(id, winery, variety, title,twh, taster, region1,region2,province,price,points,designation,description_id);
		  employeeID = (Integer) session.save(employee); 
		  tx.commit();
	   } catch (HibernateException e) {
		  if (tx!=null) tx.rollback();
		  e.printStackTrace(); 
	   } finally {
		  session.close(); 
	   }
	   return employeeID;
	}



    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value ="/saveData", method = RequestMethod.POST)
    public String saveData( @RequestParam("winery") String winery, 
                            @RequestParam("variety") String variety,
                            @RequestParam("title") String title,
                            @RequestParam("tasterTwitterHandle") String tasterTwitterHandle ,
                            @RequestParam("tasterName") String tasterName,
                            @RequestParam("region1") String region1,
                            @RequestParam("region2") String region2,
                            @RequestParam("province") String province,
                            @RequestParam("price") int price,
                            @RequestParam("points") int points,
                            @RequestParam("designation") String designation){

        try {
            factory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) { 
            System.err.println("Failed to create sessionFactory object." + ex);
            throw new ExceptionInInitializerError(ex); 
        }
                                           
        Session session = factory.openSession();
        	   Transaction tx = null;
        	   Integer employeeID = null;
               
        	   try {
                  tx = session.beginTransaction();
                  SpainWineReviews employee = new SpainWineReviews(1, winery, variety, title,tasterTwitterHandle, tasterName, region1,region2,province,price,points,designation,1);
        		  employeeID = (Integer) session.save(employee); 
        		  tx.commit();
        	   } catch (HibernateException e) {
        		  if (tx!=null) tx.rollback();
        		  e.printStackTrace(); 
        	   } finally {
        		  session.close(); 
        	   }

        return "{\"msg\":\"proslo\"}";
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value ="/getData", method = RequestMethod.POST)
    public String getData(@RequestParam("id") int id){
        SpainWineReviews employee = new SpainWineReviews();
        try {
            factory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) { 
            System.err.println("Failed to create sessionFactory object." + ex);
            throw new ExceptionInInitializerError(ex); 
        }

        Session session = factory.openSession();
	   Transaction tx = null;
	   
	   try {
		  tx = session.beginTransaction();
         employee = (SpainWineReviews)session.get(SpainWineReviews.class, id);
        System.out.print(employee.getRegion1()); 

	   } catch (HibernateException e) {
		  if (tx!=null) tx.rollback();
		  e.printStackTrace(); 
	   } finally {
		  session.close(); 
	   }
       String rez = "{\"winery\":\"" + employee.getWinery() + "\",";
       rez = rez+ "\"variety\":\"" + employee.getVariety() + "\",";
       rez = rez+ "\"title\":\"" + employee.getTitle() + "\",";
       rez = rez+ "\"tasterTwitterHandle\":\"" + employee.getTasterTwitterHandle() + "\",";
       rez = rez+ "\"tasterName\":\"" + employee.getTasterName() + "\",";
       rez = rez+ "\"region1\":\"" + employee.getRegion1() + "\",";
       rez = rez+ "\"region2\":\"" + employee.getRegion2() + "\",";
       rez = rez+ "\"province\":\"" + employee.getProvince() + "\",";
       rez = rez+ "\"price\":\"" + employee.getPrice().toString() + "\",";
       rez = rez+ "\"points\":\"" + employee.getPoints().toString() + "\",";
       rez = rez+ "\"designation\":\"" + employee.getDesignation() + "\"}";
       System.out.print(rez);
        return rez;
    }
}
